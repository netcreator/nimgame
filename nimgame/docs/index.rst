Play the Game of Nim
====================

Basic info
----------

.. toctree::
    :maxdepth: 2
    
    modules/__init__


Module list
-----------

.. autosummary::
    :caption: Module list
    :toctree: modules
    
    core
    calculations
    typedefs
    version
    playing.cli.play
    playing.cli.ansi
    playing.web.play
    playing.web.playscript
    playing.demo
    tests.testruns


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
